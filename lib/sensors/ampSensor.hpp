#ifndef AMPSENSOR_HPP
#define AMPSENSOR_HPP



class AmpSensor
{
private:
    int value;
    int pin;
 
public:
    AmpSensor();
    AmpSensor(int);
 
    int getAmp();

};
 
#endif