#include "voltSensor.hpp"
#include <Arduino.h>

// VoltSensor constructor
VoltSensor::VoltSensor(int _pin)
{
    pin = _pin;

}
 
// VoltSensor member function
int VoltSensor::getVolt()
{
   return analogRead(pin);
}