#ifndef VOLTSENSOR_HPP
#define VOLTSENSOR_HPP
 
class VoltSensor
{
private:
    int value;
    int pin;
 
public:
    VoltSensor();
    VoltSensor(int);
 
    int getVolt();

};
 
#endif