#include "../rules/rule_basic.hpp"
#include "../sensors/voltSensor.hpp"
#include "../sensors/ampSensor.hpp"

// Rule_basic constructor
Rule_basic::Rule_basic(VoltSensor _voltsensor, AmpSensor _ampSensor)
{
    voltSensor = _voltsensor;
    ampSensor = _ampSensor;
}


// Rule_basic member function
int Rule_basic::run()
{
    int t_currentVolt = voltSensor.getVolt();
    int t_currentAmp  = ampSensor.getAmp();    
    
    int power = t_currentVolt * 0.6;
    
    return power;
}