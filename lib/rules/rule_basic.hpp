#ifndef RULE_BASIC_HPP
#define RULE_BASIC_HPP
 
// #include "voltSensor.hpp"
// #include "ampSensor.hpp"

#include "../sensors/voltSensor.hpp"
#include "../sensors/ampSensor.hpp"

class Rule_basic
{
private:
    VoltSensor voltSensor;
    AmpSensor ampSensor;
 
public:
    Rule_basic();
    Rule_basic(VoltSensor, AmpSensor);
 
    int run();

};
 
#endif