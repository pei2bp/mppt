#ifndef FETACTUATOR_HPP
#define FETACTUATOR_HPP



class FetActuator
{
private:
    
    int dutyCycle;
    int pin;
 
public:
    FetActuator();
    FetActuator(int);
 
    int setDutyCycle(int dutyCycle);

};
 
#endif