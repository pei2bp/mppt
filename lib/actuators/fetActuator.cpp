#include "fetActuator.hpp"
#include <Arduino.h>

// FetActuator constructor
FetActuator::FetActuator(int _pin)
{
    int pin = _pin;
}
 
// FetActuator member function
int FetActuator::setDutyCycle(int _dutyCycle)
{
    dutyCycle = _dutyCycle;
    
    digitalWrite(pin, dutyCycle);
}