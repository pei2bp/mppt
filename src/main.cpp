#include <Arduino.h>

// #include "sensors/voltSensor.hpp"
// #include "sensors/ampSensor.hpp"
// #include "rules/rule_basic.hpp"

#include <voltSensor.hpp>
#include <ampSensor.hpp>
#include <fetActuator.hpp>
#include <rule_basic.hpp>
#include <string.h>


String STATE = "CHARGE";

int voltSensor_panel_pin = 1;
int ampSensor_panel_pin = 2;

int voltSensor_batt_pin = 3;
int ampSensor_batt_pin = 4;

int fetActuator_pin = 9;

VoltSensor voltSensor_panel   = VoltSensor(voltSensor_panel_pin);
AmpSensor ampSensor_panel     = AmpSensor(ampSensor_panel_pin);

VoltSensor voltSensor_battery = VoltSensor(voltSensor_batt_pin);
AmpSensor ampSensor_battery   = AmpSensor(ampSensor_batt_pin);

FetActuator fetActuator       = FetActuator(fetActuator_pin);



// Rule_basic basic_rule = Rule_basic(voltSensor, ampSensor);
// Rule_basic basic_rule = Rule_basic();

int rule_charge()
{
    Serial.println("State: charge");
    STATE = "CHARGED";
    int t_currentVolt = voltSensor_panel.getVolt();
    int t_currentAmp  = ampSensor_panel.getAmp();    
    
    int power = t_currentVolt * 0.6;
    
    return power;
}

int rule_charged()
{
    Serial.println("State: chareged");
    STATE = "CURR_LIMIT";
    int t_currentVolt = voltSensor_panel.getVolt();
    int t_currentAmp  = ampSensor_panel.getAmp();    
    
    int power = t_currentVolt * 0.6;
    
    return power;
}


int rule_curr_limit()
{
    Serial.println("State: current_limit");
    STATE = "DISCHARGE";
    int t_currentVolt = voltSensor_panel.getVolt();
    int t_currentAmp  = ampSensor_panel.getAmp();    
    
    int power = t_currentVolt * 0.6;
    
    return power;
}

int rule_discharge()
{
    Serial.println("State: discharege");
    STATE = "DISCHARGED";
    int t_currentVolt = voltSensor_panel.getVolt();
    int t_currentAmp  = ampSensor_panel.getAmp();    
    
    int power = t_currentVolt * 0.6;
    
    return power;
}

int rule_discharged()
{
    Serial.println("State: dischareged");
    STATE = "CHARGE";
    int t_currentVolt = voltSensor_panel.getVolt();
    int t_currentAmp  = ampSensor_panel.getAmp();    
    
    int power = t_currentVolt * 0.6;
    
    return power;
}

enum state {
  CHARGE,
  CHARGED, 
  CURR_LIMIT,
  DISCHARGE,
  DISCHARGED 
};

state hashit(String const& inString) {
    if (inString == "CHARGE") return CHARGE;
    if (inString == "CHARGED") return CHARGED;
    if (inString == "CURR_LIMIT") return CURR_LIMIT;
    if (inString == "DISCHARGE") return DISCHARGE;
    if (inString == "DISCHARGED") return DISCHARGED;
  
}

void rule_controller(String input_state)
{
  switch (hashit(input_state))
  {
    case CHARGE: rule_charge();
      break;
    case CHARGED: rule_charged();
      break;
    case CURR_LIMIT: rule_curr_limit();
      break;
    case DISCHARGE: rule_discharge();
      break;
    case DISCHARGED: rule_discharged();
      break;
    default:
      break;
  }


}

void setup() {

  Serial.begin(9600);

}

void loop() {

  rule_controller(STATE);
    
}

